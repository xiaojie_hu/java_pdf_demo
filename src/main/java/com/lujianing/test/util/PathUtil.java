package com.lujianing.test.util;

import java.io.File;

/**
 * 获取当前项目路径工具类
 */
public class PathUtil {

    /**
     * 获取当前项目java目录的classpath路径
     * 获取路径示例：D:/IDEA-WorkSpace/springboot-easyui\src\main\java
     * @return
     */
    public static String getProjectJavaClasspathPath() {
        String projectPath = getCurrentPath();
        if(getCurrentPath().indexOf("/target") != -1)
        {
            projectPath = getCurrentPath().substring(0,getCurrentPath().indexOf("/target"));
        }
        projectPath = projectPath + File.separator + "src" + File.separator + "main" + File.separator + "java";
        return projectPath;
    }

    /**
     * 获取当前项目resources目录的classpath路径
     * 获取路径示例：D:/IDEA-WorkSpace/springboot-easyui\src\main\resources
     * @return
     */
    public static String getProjectResourcesClasspathPath() {
        String projectPath = getCurrentPath();
        if(getCurrentPath().indexOf("/target") != -1)
        {
            projectPath = getCurrentPath().substring(0,getCurrentPath().indexOf("/target"));
        }
        projectPath = projectPath + File.separator + "src" + File.separator + "main" + File.separator + "resources";
        return projectPath;
    }

    /**
     * 获取路径示例：G:/IDEA-WorkSpace/java_pdf_demo/java_pdf_demo/target/classes/
     * @return
     */
    public static String getCurrentPath() {

        Class<?> caller = getCaller();
        if (caller == null) {
            caller = PathUtil.class;
        }

        return findCurrentPath(caller);
    }


    public static Class<?> getCaller() {
        StackTraceElement[] stack = (new Throwable()).getStackTrace();
        if (stack.length < 3) {
            return PathUtil.class;
        }
        String className = stack[2].getClassName();
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String findCurrentPath(Class<?> cls) {
        String path = cls.getProtectionDomain().getCodeSource().getLocation().getPath();
        path = path.replaceFirst("file:/", "");
        path = path.replaceAll("!/", "");
        if (path.lastIndexOf(File.separator) >= 0) {
            path = path.substring(0, path.lastIndexOf(File.separator));
        }
        if ("/".equalsIgnoreCase(path.substring(0, 1))) {
            String osName = System.getProperty("os.name").toLowerCase();
            if (osName.contains("window")) {
                path = path.substring(1);
            }
        }
        return path;
    }

}
